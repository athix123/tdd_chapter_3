from flask import Flask
from test_tdd.rest import room
from test_tdd.flask_settings import DevConfig


def create_app(config_object=DevConfig):
	app = Flask(__name__)
	app.config.from_object(config_object)
	app.register_blueprint(room.blueprint)
	return app