# -*- coding: utf-8 -*-

"""Console script for test_tdd."""
import sys
import click


@click.command()
def main(args=None):
    """Console script for test_tdd."""
    click.echo("Replace this message by putting your code into "
               "test_tdd.cli.main")
    click.echo("See click documentation at http://click.pocoo.org/")
    return 0


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
