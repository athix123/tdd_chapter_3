import pytest

from test_tdd.app import create_app
from test_tdd.flask_settings import TestConfig

@pytest.yield_fixture(scope='function')
def app():
	return create_app(TestConfig)